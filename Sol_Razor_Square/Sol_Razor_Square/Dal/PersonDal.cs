﻿using Sol_Razor_Square.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Razor_Square.Dal
{
    public class PersonDal
    {
        #region Declaration
        private PersonDcDataContext dc = null;
        #endregion

        #region Constructor
        public PersonDal()
        {
            dc = new PersonDcDataContext();
        }
        #endregion

        #region Public Methods
        public IEnumerable<PersonEntity> GetPersonData()
        {
            return
                dc
                ?.Persons
                ?.AsEnumerable()
                ?.Select((leLinqPersonObj) => new PersonEntity()
                {
                    BussinessId = leLinqPersonObj.BusinessEntityID,
                    FirstName = leLinqPersonObj.FirstName,
                    LastName = leLinqPersonObj.LastName
                })
                ?.Take(18)
                ?.ToList();
        }
        #endregion  
    }
}