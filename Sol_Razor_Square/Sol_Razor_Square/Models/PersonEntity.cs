﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Razor_Square.Models
{
    public class PersonEntity
    {
        public decimal BussinessId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}